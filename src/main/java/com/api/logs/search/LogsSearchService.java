package com.api.logs.search;

import com.api.logs.domain.Log;
import com.api.logs.domain.LogRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

public class LogsSearchService {

    @Autowired
    private LogRepository repository;

    @Autowired
    private LogsSearchMapper mapper;

    public List<LogsSearchResponse> searchLogs() {
        List<Log> list = repository.findAll();
        List<LogsSearchResponse> responseList = new ArrayList<>();
        for (Log log : list) {
            responseList.add(mapper.toLogsDto(log));
        }
        return responseList;
    }
}
