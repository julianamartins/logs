package com.api.logs.search;

import lombok.Data;

import javax.validation.constraints.Positive;

@Data
public class LogsSearchRequest {

    @Positive
    private Integer page = 1;
    @Positive
    private Integer pageSize = 10;
    private String orderField;
    private String orderDirection;
    private LogsSearchQueryParams queryParams;
}
