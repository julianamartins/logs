package com.api.logs.search;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class LogsSearchResponse {
    private Long id;
    private LocalDateTime timeStamp;
    private String logLevel;
    private String message;
}
