package com.api.logs.search;

import lombok.Data;

@Data
public class LogsSearchQueryParams {

    private String field;
    private String value;
}
