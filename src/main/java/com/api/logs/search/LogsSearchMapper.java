package com.api.logs.search;

import com.api.logs.domain.Log;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface LogsSearchMapper {

    LogsSearchResponse toLogsDto(Log log);
}
