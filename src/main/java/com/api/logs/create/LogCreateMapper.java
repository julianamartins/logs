package com.api.logs.create;

import com.api.logs.domain.Log;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface LogCreateMapper {

    LogCreateResponse toLogsDto(Log log);
    Log toLogs(LogCreateRequest log);
}
