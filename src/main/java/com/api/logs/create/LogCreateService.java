package com.api.logs.create;

import com.api.logs.domain.Log;
import com.api.logs.domain.LogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LogCreateService {

    @Autowired
    LogRepository repository;

    @Autowired
    LogCreateMapper mapper;

    public LogCreateResponse createLog(LogCreateRequest request) {
        Log log = mapper.toLogs(request);
        repository.save(log);
        return mapper.toLogsDto(log);
    }
}
