package com.api.logs.create;

import lombok.Data;

@Data
public class LogCreateResponse {

    private Long id;
}
