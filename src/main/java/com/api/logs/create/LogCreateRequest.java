package com.api.logs.create;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class LogCreateRequest {

    @NotNull
    private String logLevel;

    @NotNull
    private String message;
}
