package com.api.logs.create;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/logs")
public class LogCreateController {

    @Autowired
    private LogCreateService service;

    @PostMapping
    public LogCreateResponse logCreate(LogCreateRequest request) {
        return service.createLog(request);
    }
}
